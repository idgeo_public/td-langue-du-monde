## TD Langue du monde

### Manipulation d'une base de données PostGIS en mode CLI

### Objectif

Créer une base de données PostGIS qui stocke des informations sur les langues parlées par pays en utilisant psql et ogr2ogr.

### Étapes

#### 1. Récupration des données:
Téléchargez les données sur les langues et les pays depuis la source proposée et les mettre dans un dossier dédié.

#### 2. Création de la base de données:
Connectez-vous à votre serveur de base de données avec le client psql.

Créez une base de données PostgreSQL vide avec PostGIS activé. (libre à vous d'utiliser votre serveur chez Always-data)

```bash:
psql -h localhost -d postgres -U postgres;
```

```sql:
create database langage_monde;
```
	
```psql:
\c langage_monde;
```
	
	
```sql:
CREATE EXTENSION postgis;
```
	

#### 3. Modèle de données

*Utiliser looping pour envisager le MCD / MLD adéquat. "Envisager" et utiliser par la suite les bouts de SQL manquant car l'import de la couche shapefile par ogr2ogr créera la table avec les données.*
- Porposer un MCD / MLD (capture d'écran)


#### 4. Importation des données des pays:

Utilisez ogr2ogr pour importer les données des pays dans la base de données. 

Avec Powershell, vous positionner dans le bon dossier pour éxécuter la commande.
    
```bash:
	ogr2ogr -f "PostgreSQL" PG:"dbname=langage_monde host=localhost password=postgres user=postgres" -nln pays_monde -nlt PROMOTE_TO_MULTI -skipfailures .\pays_monde.shp
```

Vérifier la présence de la table et des données ainsi crée dans la base. Combien y a-t-il  d'enregistrements dans la table pays_monde ? Ecrire la requête.

**Warning**

A ce stade, la clé primaire est désignée sur fid mais sans contrainte d'unicité. Nous ajoutons cette contrainte avec la requête suivante :

```sql:
ALTER TABLE pays_monde ADD CONSTRAINT pays_monde_fid_unique UNIQUE (fid);
```

#### 5. Création des autres tables:

- Créez une table dans la base de données qui servira à stocker les informations sur les langues parlées. 

```sql:
CREATE TABLE langue(
   id_langue VARCHAR(50) ,
   name VARCHAR(250) ,
   inverse_name VARCHAR(250) ,
   PRIMARY KEY(id_langue, name)
);
```
	
- Créez la table d'association "parler",

```sql:
CREATE TABLE parler(
   fid INTEGER,
   id_langue VARCHAR(50) ,
   name VARCHAR(250) ,
   PRIMARY KEY(fid, id_langue, name),
   FOREIGN KEY(fid) REFERENCES pays_monde(fid),
   FOREIGN KEY(id_langue, name) REFERENCES langue(id_langue, name)
);
```


#### 6. Importation des données des langues:

Utilisez psql ou un autre outil pour importer les données sur les langues dans la table que vous venez de créer.

```psql:
	\COPY langue FROM 'lettre:/chemin/vers/le/bon/dossier/iso-639-3_Name_Index_20231220_ansi.csv' DELIMITER ';' CSV HEADER;
```
=> résultat attendu : COPY 8290

#### 7. Jointure entre les tables:

Effectuez les jointures entre les tables des langues et la table des pays.

```sql:
select * 
from public.pays_monde as t1 
join public.parler as t2 ON t1.fid=t2.fid
join public.langue as t3 ON t2.id_langue=t3.id_langue AND t2.name=t3.name;
```
	

#### 8. Bonus :

- Ecrire la requête permettant de déterminer la liste des langues parlées dans un pays.

=> group by 

 - Proposer une amélioration du modèle de données pour déterminer la langiue officielle par pays.
 
```sql:
 ALTER TABLE parler ADD COLUMN officiel BOOLEAN;
```
 
  - Proposer la requête qui permet de renvoyer la langue officielle parlée par pays.
  
### Sources de données 

- Fichier shapefile des pays, issue de l'ester egg world de Qgis et exporter en shp SRC WGS84 EPSG:4326

- Fichier des langues issues de https://iso639-3.sil.org/code_tables/639/data
pour une réutilisation et un usgae non commercial
iso-639-3_Name_Index_20231220.tab, renommé en iso-639-3_Name_Index_20231220.csv séparateur ;