### Correction


### Fin étape 4  

```psql:
langage_monde=# select count(*) from pays_monde;
 count
-------
   240
(1 row)```

### warning 


ALTER TABLE pays_monde ADD CONSTRAINT pays_monde_fid_unique UNIQUE (fid);






### Fin étape 6

langage_monde=# \dt
              List of relations
 Schema |      Name       | Type  |  Owner
--------+-----------------+-------+----------
 public | langue          | table | postgres
 public | parler          | table | postgres
 public | pays_monde      | table | postgres
 public | spatial_ref_sys | table | postgres
(4 rows)