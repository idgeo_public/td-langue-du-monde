CREATE TABLE pays_monde(
   fid INTEGER,
   iso_a2 VARCHAR(50) ,
   name VARCHAR(50) ,
   fips_10_ VARCHAR(50) ,
   iso_a3 VARCHAR(50) ,
   wb_a2 VARCHAR(50) ,
   wb_a3 VARCHAR(50) ,
   geom GEOMETRY (MULTIPOLYGON,4326),
   PRIMARY KEY(fid)
);

CREATE TABLE langue(
   id_langue VARCHAR(50) ,
   name VARCHAR(250) ,
   inverse_name VARCHAR(250) ,
   PRIMARY KEY(id_langue, name)
);

CREATE TABLE parler(
   fid INTEGER,
   id_langue VARCHAR(50) ,
   name VARCHAR(250) ,
   PRIMARY KEY(fid, id_langue, name),
   FOREIGN KEY(fid) REFERENCES pays_monde(fid),
   FOREIGN KEY(id_langue, name) REFERENCES langue(id_langue, name)
);
